﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RspWeb.Contracts;
using RspWeb.Models;
using System.Collections.Generic;
using Moq;
using RspWeb.Controllers;
using System.Web.Http.Results;
using System.Linq;
using GeorgeCloney;

namespace RspWebTests
{
    [TestClass]
    public class InsurancePolicyControllerTests
    {
        private Mock<IInsurancePolicyRepository> _mockPolicyRepo = new Mock<IInsurancePolicyRepository>();
        private InsurancePolicyController _controller;
        private const string _requestUri = "http://localhost/api/insurancepolicy/";
        private List<InsurancePolicy> _policies = new List<InsurancePolicy>();

        public InsurancePolicyControllerTests()
        {
            _controller = new InsurancePolicyController(_mockPolicyRepo.Object);

            _controller.Request = new System.Net.Http.HttpRequestMessage
            {
                RequestUri = new Uri("http://localhost/api/insurancepolicy/")
            };

        }

        [TestInitialize]
        public void Init()
        {
            InsurancePolicy ip1 = new InsurancePolicy
            {
                Id = 0,
                Number = Guid.NewGuid().ToString().Replace('-', ' '),
                Effective = DateTime.Now,
                Expiration = DateTime.Now.AddDays(365),
                Insureds = new List<Insured>
                {
                    new Insured
                    {
                        Name = "Jim Geeks",
                    Address = "1313 Mockingbird Lane",
                    City = "Houston",
                    State = "Texas",
                    ZipCode = "77002",
                    IsPrimary = true
                    }
                },

                Risks = new List<Risk>
                {
                    new Risk
                    {

                    Construction = RiskConstructionType.DoubleWide,
                    YearBuilt = 2003,
                    Address = "1313 Mockingbird Lane",
                    City = "Houston",
                    State = "Texas",
                    ZipCode = "77002"
                    }
                }
            };

            InsurancePolicy ip2 = new InsurancePolicy
            {
                Id = 1,
                Number = Guid.NewGuid().ToString().Replace('-', ' '),
                Effective = DateTime.Now,
                Expiration = DateTime.Now.AddDays(365),
                Insureds = new List<Insured>
                {
                    new Insured
                    {
                     Name = "Johm Winter",
                    Address = "1313 Mockingbird Lane",
                    City = "Houston",
                    State = "Texas",
                    ZipCode = "77002",
                    IsPrimary = true
                    }
                },

                Risks = new List<Risk>
                {
                    new Risk
                    {

                    Construction = RiskConstructionType.DoubleWide,
                    YearBuilt = 2003,
                    Address = "1313 Mockingbird Lane",
                    City = "Houston",
                    State = "Texas",
                    ZipCode = "77002"
                    }
                }
            };


            _policies.Add(ip1);
            _policies.Add(ip2);
        }

        [TestMethod]
        public async Task ShouldBeAbleToAddNewInsurancePolicy()
        {

            List<InsurancePolicy> policies = new List<InsurancePolicy>();
            bool addPolicyCalled = false;

            var policy2Add = _policies[0].DeepClone();

            _mockPolicyRepo.Setup(m => m.AddPolicy(It.IsAny<InsurancePolicy>())).Returns(() =>
            {
                addPolicyCalled = true;
                var t = new TaskCompletionSource<InsurancePolicy>();
                policy2Add.Id = 0;
                t.SetResult(policy2Add);

                return t.Task;

            });

            var result = await _controller.Post(policy2Add);
            var contentResult = result as CreatedNegotiatedContentResult<InsurancePolicy>;

            var addedPolicy = contentResult.Content;
            var resourceLocation = contentResult.Location;

            Assert.IsTrue(addPolicyCalled);
            Assert.AreEqual(new Uri(string.Format("{0}{1}", _requestUri, addedPolicy.Id)), resourceLocation);
            Assert.AreEqual(0, addedPolicy.Id);
            Assert.AreEqual(policy2Add.Number, addedPolicy.Number);
            Assert.AreEqual(policy2Add.Effective, addedPolicy.Effective);

            Assert.AreEqual(policy2Add.Expiration, addedPolicy.Expiration);
            Assert.AreEqual(policy2Add.Insureds, addedPolicy.Insureds);
            Assert.AreEqual(policy2Add.Risks, addedPolicy.Risks);


        }

        [TestMethod]
        public async Task ShouldBeAbleToRetriveAllExistingInsurancePolicies()
        {
                       

            _mockPolicyRepo.Setup(m => m.GetAll()).ReturnsAsync(_policies);

            var existingPolicies  = await _controller.Get();
            
            Assert.IsNotNull(existingPolicies);
            Assert.AreEqual(2, existingPolicies.Count());

            CollectionAssert.AreEqual(_policies, existingPolicies.ToList());

        }

        [TestMethod]
        public async Task ShouldBeAbleToUpdateExistingInsurancePolicies()
        {

            Func<InsurancePolicy, Task<InsurancePolicy>> UpdatePolicy = (InsurancePolicy ip) =>
            {
                var policy = _policies.FirstOrDefault(p => p.Id == ip.Id);

                policy.Expiration = ip.Expiration;
                policy.Effective = ip.Effective;
                policy.Insureds = ip.Insureds;
                policy.Risks = ip.Risks;

                return Task.FromResult(policy);
            };

            // _mockPolicyRepo.Setup(m => m.GetById(It.IsAny<int>())).Returns((int id) => GetPolicyById(id)); 
            _mockPolicyRepo.Setup(m => m.Update(It.IsAny<InsurancePolicy>())).Returns((InsurancePolicy ip) => UpdatePolicy(ip));

            InsurancePolicy ip2Update = _policies[1].DeepClone();

            ip2Update.Insureds = new List<Insured>{
                new Insured
                    {
                     Name = "Jim Galving",
                    Address = "1313 Mockingbird Lane",
                    City = "Houston",
                    State = "Texas",
                    ZipCode = "77002",
                    IsPrimary = true
                    }
                };

            ip2Update.Risks = new List<Risk>();


            var result = await _controller.Put(ip2Update);
            var contentResult = result as OkNegotiatedContentResult<InsurancePolicy>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(ip2Update, contentResult.Content);


        }


        [TestMethod]
        public async Task ShouldBeAbleToDeleteExistingInsurancesPolicies()
        {


            Func<int, Task> DeleteId = (int id)=>
            {
                return Task.Factory.StartNew(() => {


                    var deletePolicy = _policies.FirstOrDefault(p => p.Id == id);
                    _policies.Remove(deletePolicy);

                });
            };

            _mockPolicyRepo.Setup(m => m.Delete(It.IsAny<int>())).Returns((int id) => DeleteId(id));             
          

            var policy2Delete = _policies[0];

            var result = await _controller.Delete(policy2Delete.Id);
           
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkResult));
            Assert.AreEqual(1, _policies.Count);

            var deletedPolicy = _policies.FirstOrDefault(p => p.Id == 0);

            Assert.IsNull(deletedPolicy);
        }
    }
}
