﻿using Autofac;
using RspWeb.Contracts;
using RspWeb.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RspWeb
{
    public class ServiceModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(ip => new InsurancePolicyRepository()).As<IInsurancePolicyRepository>();
            
        }
    }
}