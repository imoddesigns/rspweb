﻿using RspWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RspWeb.Contracts
{
    public interface IInsurancePolicyRepository : IDisposable
    {
        Task<InsurancePolicy> AddPolicy(InsurancePolicy policy);
        Task<IEnumerable<InsurancePolicy>> GetAll();

        Task<InsurancePolicy> GetById(int id);

        Task<InsurancePolicy> Update(InsurancePolicy policy);

        Task Delete(int id);

        Task<IEnumerable<InsurancePolicy>> Find(PolicySearch search);
    }
}
