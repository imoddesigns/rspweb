﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RspWeb.Models
{
    public class Insured
    {
        //- Primary Insured Name
        //- Primary Insured Mailing Address
        //- Primary Insured City
        //- Primary Insured State
        //- Primary Insured Zip Code

        public int Id { get; set; }

        public bool IsPrimary { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public override bool Equals(object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;
            Insured insured = (Insured)obj;

            return (Name == insured.Name) &&
                (Address == insured.Address) &&
                (City == insured.City) &&
                (State == insured.State) &&
                (ZipCode == insured.ZipCode);

        }

        public override int GetHashCode()
        {
            return Name.Count().GetHashCode();
        }
    }
}