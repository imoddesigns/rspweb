﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RspWeb.Models
{
    //    - Policy Number
    //- Policy Effective Date
    //- Policy Expiration Date



    public class InsurancePolicy
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public DateTime Effective { get; set; }
        public DateTime Expiration { get; set; }

        [NotMapped]
        public string PrimaryName
        {

            get
            {

                var primaryName = string.Empty;

                var primaryInsured = Insureds.FirstOrDefault(i => i.IsPrimary);

                if (primaryInsured != null)
                    return primaryInsured.Name;
                else
                {

                    return string.Empty;
                }
             }
        }

        public List<Insured> Insureds { get; set; }

        public List<Risk> Risks { get; set; }


        public InsurancePolicy()
        {
            Insureds = new List<Insured>();
            Risks = new List<Risk>();
        }

        public override bool Equals(object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            InsurancePolicy policy = (InsurancePolicy)obj;

            var insuredsMatch = true;
            var myInsuredsCount = Insureds.Count;
            var yourInsuredsCount = policy.Insureds.Count;

            if (myInsuredsCount == yourInsuredsCount)
            {
                for (int i = 0; i < myInsuredsCount; i++)
                {
                    var myInsureds = Insureds[i];
                    var yourInsureds = policy.Insureds[i];

                    if (!myInsureds.Equals(yourInsureds))
                    {
                        insuredsMatch = false;
                        break;
                    }
                }

            }
            else
            {
                insuredsMatch = false;
            }


            var risksMatch = true;
            var myRisksCount = Risks.Count;
            var yourRisksCount = policy.Risks.Count;

            if (myRisksCount == yourRisksCount)
            {
                for (int i = 0; i < myRisksCount; i++)
                {
                    var myRisk = Risks[i];
                    var yourRisk = policy.Risks[i];

                    if (!myRisk.Equals(yourRisk))
                    {
                        risksMatch = false;
                        break;
                    }
                }

            }
            else
            {
                risksMatch = false;
            }


            return (Id == policy.Id) &&
                (Number == policy.Number) &&
                (Effective == policy.Effective) &&
                (Expiration == policy.Expiration) &&
                (insuredsMatch) &&
                (risksMatch);

        }

        public override int GetHashCode()
        {
            return Id ^ Insureds.Count ^ Risks.Count;
        }

    }

    public enum RiskConstructionType
    {

        SiteBuilt,
        Modular,
        SingleWide,
        DoubleWide
    }
}
