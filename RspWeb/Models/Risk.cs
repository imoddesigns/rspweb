﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RspWeb.Models
{
    public class Risk
    {
        //- Risk Construction *(must be one of four values: Site Built Home, Modular Home, Single Wide Manufactured Home, Double Wide Manufactured Home)*
        //- Risk Year Built
        //- Risk Address
        //- Risk City
        //- Risk State
        //- Risk Zip Code

        public int Id { get; set; }

        public RiskConstructionType Construction { get; set; }

        public int YearBuilt { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public override bool Equals(object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            Risk risk = (Risk)obj;

            return (Construction == risk.Construction) &&
               (YearBuilt == YearBuilt) &&
               (Address == Address) &&
               (City == City) &&
               (State == State) &&
               (ZipCode == ZipCode);


        }

        public override int GetHashCode()
        {
            return City.GetHashCode() ^ Address.GetHashCode();
        }
    }
}