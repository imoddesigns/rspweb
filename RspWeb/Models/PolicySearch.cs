﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RspWeb.Models
{
    public class PolicySearch
    {
        public string Criteria { get; set; }
        public bool? UsePolicyNumber { get; set; }
        public bool? UseInsuredName { get; set; }
    }
}