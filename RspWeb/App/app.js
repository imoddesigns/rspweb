﻿
'use strict';

var appConfig = function ($routeProvider, localStorageServiceProvider, $mdToastProvider) {

    localStorageServiceProvider
    .setPrefix('rpsWeb');


    $mdToastProvider.addPreset('updatePreset', {
        options: function () {
            return {
                template:
                  '<md-toast>' +
                    '<div class="md-toast-content">' +
                      'updated successfully' +
                    '</div>' +
                  '</md-toast>',
                controllerAs: 'toast',
                bindToController: true
            };
        }
    }).addPreset('errorPreset', {
        options: function () {
            return {
                template:
                  '<md-toast>' +
                    '<div class="md-toast-content">' +
                      '<i style="color:red"class="material-icons">error</i>' +
                      'an error occurred' +
                    '</div>' +
                  '</md-toast>',
                controllerAs: 'toast',
                bindToController: true
            };
        }
    }).addPreset('loginErrorPreset', {

        options: function () {
            return {
                template:
                  '<md-toast>' +
                    '<div class="md-toast-content">' +
                      '<i style="color:red"class="material-icons">error</i>' +
                      'login error: please check username and password' +
                    '</div>' +
                  '</md-toast>',
                controllerAs: 'toast',
                bindToController: true
            };
        }
    }).addPreset('registrationErrorPreset', {

        options: function () {
            return {
                template:
                  '<md-toast>' +
                    '<div class="md-toast-content">' +
                      '<i style="color:red"class="material-icons">error</i>' +
                      'registration: error occurred during registration process' +
                    '</div>' +
                  '</md-toast>',
                controllerAs: 'toast',
                bindToController: true
            };
        }
    });


    $routeProvider
    .when("/login", {

        controller: 'LoginController',
        templateUrl: 'views/login.html'
    })
    .when("/register", {
         
        controller: 'RegisterController',
        templateUrl: 'views/register.html'

      })
        .when("/", {

        controller: 'PoliciesController',
        templateUrl: 'views/policies.html'
    }).
    when("/policies", {

        controller: 'PoliciesController',
        templateUrl: 'views/policies.html'
    }).
    when("/viewpolicies/:id", {

        controller: 'ViewPolicyController',
        templateUrl: 'views/viewpolicy.html'
    }).
    when("/editpolicy/:id", {

        controller: 'EditPolicyController',
        templateUrl: 'views/editpolicy.html'
    }).
    when("/createnewpolicy", {
        controller: 'CreatePolicyController',
        templateUrl: 'views/createpolicy.html'
    }).when("/destroy", {

        controller: 'LogOffController',
        templateUrl: 'views/dummyview.html'

    }).when("/notfound", {

      
        templateUrl: 'views/notfound.html'

    });
}

    var authRouteHandler = function (next, location) {

        if (next.templateUrl === "views/register.html")
        {
            //location.path("/register");
        }
        else {

            location.path("/login");
        }


    }



    var app = angular.module('app', [
        // Angular modules
        'ngAnimate',
        'ngRoute',
        'ngMaterial', 
        'ngMessages',
        'LocalStorageModule',
        'angularModalService',
        'ui.bootstrap'

        // Custom modules

        // 3rd Party Modules

    ]).config(appConfig).run(function ($rootScope, $location, AuthService) {

        $rootScope.showNavbar = true;

        $rootScope.$on("$routeChangeStart", function (event, next, current) {

    
            var isUserLoggedIn = AuthService.isUserLoggedIn();

            $rootScope.showNavbar = isUserLoggedIn;


            if (!isUserLoggedIn) {

                authRouteHandler(next, $location);

            }
    

        });

    });

    app.filter('nameorpolicynumber', function () {

        // In the return function, we must pass in a single parameter which will be the data we will work on.
        // We have the ability to support multiple other parameters that can be passed into the filter optionally
        return function (input, criteria) {

            var output = [];
            var searchCriteria;

            if (criteria.primaryName == '' && criteria.number == '') 
            {
                return input;
            }
            else if (criteria.primaryName != '')
            {
                //return all names that match criteria
                searchCriteria = criteria.primaryName.toLowerCase();

                var i = 0;
                for (i ;i < input.length; i++)
                {
                    var includeInResults = false;

                    //get insureds
                    var insureds = input[i].insureds;

                    if(insureds)
                    {
                        var j = 0;
                        for(j; j < insureds.length; j++)
                        {
                            var name = insureds[j].name.toLowerCase();;

                            if(name.startsWith(searchCriteria))
                            {
                                includeInResults = true;
                                break;
                            }
                        }
                    }

                    if (includeInResults) {
                        output.push(input[i]);
                    }
                }

               
            }
            else if (criteria.number != '')
            {
                //return all names that match criteria
                searchCriteria = criteria.number.toLowerCase();

                var i = 0;
                for (i ; i < input.length; i++) {

                    var number = input[i].number;

                    if(number.startsWith(searchCriteria))
                    {
                        output.push(input[i]);
                    }
                }
            }
            else {
                return input;
            }

            // Do filter work here
            return output;
        

        }

    });