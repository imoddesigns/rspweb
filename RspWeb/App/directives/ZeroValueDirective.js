﻿
app.directive('zerolength', function () {

    var linkFunction = function (scope, elem, attrs, ctrl) {
        if (!ctrl) return;

        // force truthy in case we are on non input element

        ctrl.$validators.zerolength = function (modelValue, viewValue) {

            if(modelValue)
            {
                return modelValue.length > 0;
            }
        };

        attrs.$observe('zerolength', function () {
            ctrl.$validate();
        });
    };

    return {


        restrict: 'A',
        require: 'ngModel',
        compile: function (elem, attrs) {
            // compile coded
            return linkFunction;
        }
       
    }
});
