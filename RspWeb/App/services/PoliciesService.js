﻿app.service('PoliciesService', function ($http, $q, AuthService, localStorageService) {


    var policies = [];


    this.getAll = function () {

        var deferred = $q.defer();

        $http.get('api/insurancepolicy', {
            headers: {
                "Accept": 'application/json',
                "Content-Type": 'application/json',
                "Authorization": 'Bearer ' + AuthService.getAccessToken()
            }
        }).success(function (data) {

            policies = [];

            data.forEach(function (val, i) {
               policies.push(val);
            });

            deferred.resolve(policies);
        }).error(deferred.reject);
        return deferred.promise;
    }

    this.getPolicyById = function (id)
    {
        var deferred = $q.defer();

        $http.get('api/insurancepolicy/' + id, {
            headers: {
                "Accept": 'application/json',
                "Content-Type": 'application/json',
                "Authorization": 'Bearer ' + AuthService.getAccessToken()
            }
        }).success(function (data) {

            deferred.resolve(data);
        }).error(deferred.reject);
        return deferred.promise;
    }

    this.create = function (policy) {
        var deferred = $q.defer();

        $http.post('api/insurancepolicy', policy, {
            headers: {
                "Accept": 'application/json',
                "Content-Type": 'application/json',
                "Authorization": 'Bearer ' + AuthService.getAccessToken()
            }
        }
            ).success(function (data) {
                deferred.resolve(data);
            }).error(deferred.reject);
        return deferred.promise;

    }

    this.update = function(policy)
    {
        var deferred = $q.defer();

        $http.put('api/insurancepolicy', policy, {
            headers: {
                "Accept": 'application/json',
                "Content-Type": 'application/json',
                "Authorization": 'Bearer ' + AuthService.getAccessToken()
            }
        }
           ).success(function (data) {
               deferred.resolve(data);
           }).error(deferred.reject);
        return deferred.promise;
    }

    this.delete = function(policyId)
    {
        var deferred = $q.defer();

        $http.delete('api/insurancepolicy/'+ policyId, {
            headers: {
                "Accept": 'application/json',
                "Content-Type": 'application/json',
                "Authorization": 'Bearer ' + AuthService.getAccessToken()
            }
        }
           ).success(function (data) {
               deferred.resolve(data);
           }).error(deferred.reject);
        return deferred.promise;
    }


    this.search = function(criteria)
    {

        var deferred = $q.defer();

        var searchString = "?criteria=" + criteria.input;

        searchString += criteria.usePolicyNumber ? "&usepolicynumber=true" : '';
        searchString += criteria.useInsuredName ? "&useinsuredname=true" : '';
        
    
        $http.get('api/insurancepolicy/find/' + searchString, {
            headers: {
                "Accept": 'application/json',
                "Content-Type": 'application/json',
                "Authorization": 'Bearer ' + AuthService.getAccessToken()
            }
        }
           ).success(function (data) {
               deferred.resolve(data);
           }).error(deferred.reject);
        return deferred.promise;
    }

});