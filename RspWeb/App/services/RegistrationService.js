﻿app.service('RegistrationService', function ($http, $q, AuthService, localStorageService) {

    
    this.register = function (registerModel) {
        var deferred = $q.defer();

        $http.post('api/Account/Register', registerModel, {}
            ).success(function (data) {
                deferred.resolve(data);
            }).error(deferred.reject);
        return deferred.promise;

    }



});