﻿app.service('AuthService', function ($http, $q, $location, localStorageService) {



    var getAuthToken = function () {

        var authToken = localStorageService.get('authtoken');

        return authToken;
    }

    this.login = function (username, password) {

        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: '/token',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: {

                grant_type: 'password',
                username: username,
                password: password

            }
        }).success(function (data) {
            deferred.resolve(data);
        }).error(deferred.reject);
        return deferred.promise;

    }


    this.logOut = function () {

        
        this.clearAuthToken();
        $location.path('/login');
    }

    this.isUserLoggedIn = function () {
        
        var authToken = getAuthToken();

        if (authToken == null)
            return false;

        var today = moment();
        var tokenExpiration = moment(authToken.Expiration);

        var duration = moment.duration(tokenExpiration.diff(today));
        var seconds = duration.asSeconds();

        return (seconds > 0)
           
    }

    this.clearAuthToken = function () {

        localStorageService.remove('authtoken');
       
    }

    this.getAccessToken = function () {

        var authToken = getAuthToken();

        if (authToken == null)
            return null;

        return authToken.AccessToken;


    }

    this.getAuthUser = function () {

        var token = getAuthToken();

        if (token)
            return token.UserName;

    }

});