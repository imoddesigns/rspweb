﻿app.controller('PoliciesController',
  function ($scope, $location, $routeParams, $location, $mdDialog, $uibModal, PoliciesService, ModalService) {

      $scope.policies = [];

      $scope.loading = true;

      PoliciesService.getAll().then(function (policies) {

          $scope.policies = policies;

          $scope.loading = false;

      }).catch(function (data, status) {
          console.error(data);

          $scope.loading = false;
      });


      $scope.viewPolicyDetails = function (policyId) {
          $location.path('/viewpolicies/' + policyId)
      }


     
      $scope.filter = "primaryName";
      $scope.search = {
          primaryName: '',
          number: ''
      };

      $scope.changeFilterTo = function (pr) {


          $scope.filter = pr;
          $scope.search = {
              primaryName: '',
              number: ''
          };
      }

  

      $scope.createNewPolicy = function (ev) {

          $location.path('/createnewpolicy');
      }


  }
);