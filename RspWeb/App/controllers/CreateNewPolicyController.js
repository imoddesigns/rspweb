﻿app.controller('CreateNewPolicyController', function ($scope, $mdDialog, PoliciesService) {

    $scope.activeTab = 0;


    $scope.cancel = function () {

        $scope.$dismiss('cancel');
    }


    $scope.getNextTab = function () {

        $scope.activeTab += 1;
    }
});