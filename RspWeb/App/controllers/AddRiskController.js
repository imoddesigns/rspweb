﻿app.controller('AddRiskController',
  function ($scope, $rootScope, $location, $mdDialog, locals) {

      $scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
   'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
   'WY').split(' ').map(function (state) {
       return {
           abbrev: state
       };
   });


      $scope.years = [];

      var i = 1900;
      var curr = new Date().getFullYear() + 2;

      for (i; i < curr; i++) {
          $scope.years.push({
              value: i
          })

      }


      
    
      $scope.risk = new function () {


          var self = this;
          self.id;
          self.construction = 0;
          self.yearBuilt = 1970;
          self.address = '';
          self.city = '';
          self.state = '';
          self.zipCode = '';
         
      }


      if (locals.risk)
      {
          $scope.risk = locals.risk;
      }


      $scope.saveRisk = function (form) {
          if (form.$valid) {
              $mdDialog.hide($scope.risk);
          }
      }

      $scope.close = function () {
          $mdDialog.cancel();
      };



  });