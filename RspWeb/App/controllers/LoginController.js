﻿app.controller('LoginController',
  function ($scope, $rootScope, $location, $routeParams, $mdToast, AuthService, localStorageService) {

      $scope.username = '';
      $scope.password = '';

      $scope.loggingIn = false;



      $scope.login = function () {

          $scope.loggingIn = true;

          AuthService.login($scope.username, $scope.password).then(function (response) {

              var tokenExpiration = moment().add(response.expires_in, 's').toDate();
              var authToken = new AuthToken(response.access_token, tokenExpiration, $scope.username);
              localStorageService.set('authtoken', authToken);

              $rootScope.isUserLoggedIn = AuthService.isUserLoggedIn;

              $scope.loggingIn = false;

              $location.path("/policies");

          }).catch(function (error) {

              $scope.loggingIn = false;
              console.error(error);

              $mdToast.show($mdToast.loginErrorPreset().position('bottom right')
     .hideDelay(3000));
          });
      }
  }
);