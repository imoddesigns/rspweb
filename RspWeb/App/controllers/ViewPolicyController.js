﻿app.controller('ViewPolicyController', function ($scope, $location, $routeParams, $mdToast, $mdDialog, PoliciesService) {


    $scope.resourceFound = false;
    $scope.resourceNotFound = false;
    $scope.canEdit = false;
    $scope.updating = false;

    $scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
      'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
      'WY').split(' ').map(function (state) {
          return {
              abbrev: state
          };
      });

    $scope.years = [];

    var i = 1900;
    var curr = new Date().getFullYear() + 2;

    for (i; i < curr; i++) {
        $scope.years.push({
            value: i
        })

    }



    //$scope.update = function () {

    //    $scope.updating = true;

    //    PoliciesService.update($scope.policy).then(function (response) {

    //        $scope.updating = false;

    //        $mdToast.show($mdToast.updatePreset().position('bottom right')
    //    .hideDelay(3000));


    //    }).catch(function (error) {

    //        $scope.updating = false;

    //        console.error(error);

    //        $mdToast.show($mdToast.errorPreset().position('bottom right')
    //    .hideDelay(3000));
    //    });
    //}

    $scope.promptDelete = function (ev) {

        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Are you sure you want to remove this policy?')
              .textContent('Confirm deletion.')
              .ariaLabel('Confirm deletion')
              .targetEvent(ev)
              .ok('Yes')
              .cancel('Cancel');

        $mdDialog.show(confirm).then(function () {

            $scope.updating = true;


            PoliciesService.delete($scope.policy.id).then(function (response) {
                $scope.updating = false;
                $mdToast.show($mdToast.updatePreset().position('bottom right')
            .hideDelay(3000));

                $location.path('/policies');

            }).catch(function (error) {

                $scope.updating = false;

                console.error(error);

                $mdToast.show($mdToast.errorPreset().position('bottom right')
            .hideDelay(3000));
            });

        }, function () {
            //cancel
        });

    }

    $scope.redirectToEdit = function () {

        $location.path('/editpolicy/' + policyId);
    }


    var policyId = Number($routeParams.id);


    if (!isNaN(policyId)) {
        PoliciesService.getPolicyById(policyId).then(function (policy) {

            //transform the dates to javascript dates
            policy.effective = new Date(policy.effective);
            policy.expiration = new Date(policy.expiration);

            $scope.policy = policy;
            $scope.policy.effective
            $scope.resourceFound = true;

        }).catch(function (error) {

            $scope.resourceNotFound = true;
            console.error(error);
            $location.path('/notfound')
        });
    }
    else {
         
        $location.path('/notfound')
    }


});