﻿app.controller('RegisterController',
  function ($scope, $rootScope, $location,  $mdToast, AuthService, RegistrationService) {

   
      if (AuthService.isUserLoggedIn())
          $location.path('/');

      $scope.creating = false;
      $scope.username;
      $scope.password;
      $scope.confirmPassword;


      $scope.register = function () {

          $scope.creating = true;
          var registerModel = new RegisterModel($scope.username, $scope.password, $scope.confirmPassword);

          RegistrationService.register(registerModel).then(function (response) {

              $scope.creating = false;
             $location.path('/login');

          }).catch(function (error) {

              $mdToast.show($mdToast.registrationErrorPreset().position('bottom right')
        .hideDelay(3000));


          });

      }
         
  });