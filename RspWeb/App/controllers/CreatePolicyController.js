﻿app.controller('CreatePolicyController', function ($scope, $location, $mdToast, $mdDialog, $uibModal, PoliciesService) {


    $scope.savingData = false;
    $scope.savable = true;

    $scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
      'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
      'WY').split(' ').map(function (state) {
          return {
              abbrev: state
          };
      });


    $scope.selectedIndex = 0;

    $scope.cancel = function () {

        $location.path('/policies');
    }

    $scope.save = function () {

        $scope.savingData = true;


        PoliciesService.create($scope.policy).
            then(function (response) {

                $scope.savingData = false;

                $mdToast.show($mdToast.updatePreset().position('bottom right')
          .hideDelay(3000));


                $location.path('/policies');

            })
            .catch(function (data) {

                $scope.savingData = false;


                console.error(data);

                $mdToast.show($mdToast.errorPreset().position('bottom right')
          .hideDelay(3000));



            });
    }


    $scope.years = [];

    var i = 1900;
    var curr = new Date().getFullYear() + 2;

    for (i; i < curr; i++) {
        $scope.years.push({
            value: i
        })

    }


    $scope.currentNavItem = "page1";

    $scope.isActivePage = function (number) {

        return $scope.currentNavItem == "page" + number;
    }



    $scope.getFormsValid = function () {


        return (primaryInsuredForm.$valid && primaryInsuredForm.$dirty) &&
            (policyInfoForm.$valid && policyInfoForm.$dirty) &&
            (riskInfoForm.$valid && riskInfoForm.$dirty);


    }

    $scope.getNextPage = function (form) {


        angular.forEach(form.$error.required, function (field) {
            field.$setDirty();
        });

        angular.forEach(form.$error.zerolength, function (field) {
            field.$setDirty();
        });


        var formName = form.$name;

        if (formName == "primaryInsuredForm") {


            //force validation on hidden field
            form.insureds.$validate();

        }

        if (formName == "riskForm") {
            form.risks.$validate();
        }

        if (form.$valid) {

            var pgNum = Number($scope.currentNavItem.split('page')[1]);
            pgNum += 1;

            var newPage = 'page' + pgNum;

            console.log(newPage);

            $scope.currentNavItem = newPage;
        }
    }

    $scope.policy = new function () {

        var self = this;
        self.number;
        self.effective;
        self.expiration;

        self.insureds = [];

        self.risks = [];

    }

    $scope.addInsured = function (ev) {


        var locals = new function () {
            var self = this;
            self.insuredsCount = $scope.policy.insureds.length;
            self.mode = 'add';

        }


        $mdDialog.show({
            controller: 'AddInsuredController',
            templateUrl: 'views/addinsured.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            escapeToClose: false,
            locals: locals,
            fullscreen: true // Only for -xs, -sm breakpoints.
        }).then(function (insured) {

            if (insured.isPrimary) {
                angular.forEach($scope.policy.insureds, function (value, key) {

                    if (value.isPrimary == true) {
                        value.isPrimary = false;

                    }
                });



                if ($scope.policy.insureds.length > 0) {

                    $scope.policy.insureds.splice(0, 0, insured);
                    return;
                }

            }

            $scope.policy.insureds.push(insured);

        }, function () {
            console.log('You cancelled the dialog.');
        });
    }

    $scope.editInsured = function (ev, index) {
        var locals = new function () {
            var self = this;
            self.insuredsCount = $scope.policy.insureds.length;
            self.insured = $scope.policy.insureds[index];
            self.mode = 'edit';
        }

        $mdDialog.show({
            controller: 'AddInsuredController',
            templateUrl: 'views/addinsured.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            escapeToClose: false,
            locals: locals,
            fullscreen: true // Only for -xs, -sm breakpoints.
        }).then(function (insured) {

            if (insured.isPrimary) {
                angular.forEach($scope.policy.insureds, function (value, key) {

                    if (value.isPrimary == true && key != index) {
                        value.isPrimary = false;
                    }
                });


                if ($scope.policy.insureds.length > 0 && index != 0) {

                    $scope.policy.insureds.splice(index, 1);
                    $scope.policy.insureds.splice(0, 0, insured);
                }


            }


        }, function () {
            console.log('You cancelled the dialog.');
        });
    }

    $scope.addRisk = function (ev) {


        var locals = new function () {
            var self = this;
            self.risksCount = $scope.policy.risks.length;

        }


        $mdDialog.show({
            controller: 'AddRiskController',
            templateUrl: 'views/addrisk.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            escapeToClose: false,
            locals: locals,
            fullscreen: true // Only for -xs, -sm breakpoints.
        }).then(function (risk) {


            $scope.policy.risks.push(risk);

        }, function () {
            console.log('You cancelled the dialog.');
        });
    }


    $scope.editRisk = function (ev, index) {


        var locals = new function () {
            var self = this;
            self.risksCount = $scope.policy.risks.length;
            self.risk = $scope.policy.risks[index];
        }


        $mdDialog.show({
            controller: 'AddRiskController',
            templateUrl: 'views/addrisk.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            escapeToClose: false,
            locals: locals,
            fullscreen: true // Only for -xs, -sm breakpoints.
        }).then(function (risk) {

            //updated in scope

        }, function () {
            console.log('You cancelled the dialog.');
        });
    }

    $scope.promptDeleteInsured = function (ev, indexOfInsured) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Are you sure you want to remove this insured?')
              .textContent('Confirm deletion.')
              .ariaLabel('Confirm deletion')
              .targetEvent(ev)
              .ok('Yes')
              .cancel('Cancel');

        $mdDialog.show(confirm).then(function () {

            $scope.policy.insureds.splice(indexOfInsured, 1);

        }, function () {



        });
    }

    $scope.promptDeleteRisk = function (ev, indexOfRisk) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Are you sure you want to remove this risk?')
              .textContent('Confirm deletion.')
              .ariaLabel('Confirm deletion')
              .targetEvent(ev)
              .ok('Yes')
              .cancel('Cancel');

        $mdDialog.show(confirm).then(function () {

            $scope.policy.risks.splice(indexOfRisk, 1);

        }, function () {



        });
    }

    var constructionTypes = ['Site Built Home', 'Modular Home', 'Single Wide Manufactured Home', 'Double Wide Manufactured Home'];

    $scope.constructionType = function () {
        return constructionTypes[$scope.policy.riskConstruction];
    }

    $scope.constructionType = function (ndx) {
        if (ndx) {
            return constructionTypes[ndx.construction];
        }
    }

});
