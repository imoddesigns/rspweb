﻿app.controller('AddInsuredController',
  function ($scope, $rootScope, $location, $mdDialog, locals) {

      $scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
   'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
   'WY').split(' ').map(function (state) {
       return {
           abbrev: state
       };
   });

      $scope.isPrimaryEditable = true;
     
      $scope.insured = new function (){

          var self = this;
          self.id;
          self.name = '';
          self.address ='';
          self.city  = '';
          self.state  ='';
          self.zipCode = '';
          self.isPrimary;
      }

      if (locals.insuredsCount == 0)
      {
          $scope.insured.isPrimary = true;
          $scope.isPrimaryEditable = false;
      }

      if (locals.insured) {
          $scope.insured = locals.insured;
      }

      if ($scope.insured.isPrimary && locals.mode == 'edit')
      {
          $scope.isPrimaryEditable = false;
      }

    


      $scope.saveInsured = function(form)
      {
          if(form.$valid)
          {
              $mdDialog.hide($scope.insured);
          }
      }

      $scope.close = function () {
          $mdDialog.cancel();
      };



  });