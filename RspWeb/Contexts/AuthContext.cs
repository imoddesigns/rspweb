﻿using Microsoft.AspNet.Identity.EntityFramework;
using RspWeb.Contexts;
using RspWeb.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RspWeb.Contexts
{
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        public DbSet<InsurancePolicy> Policies { get; set; }

        public AuthContext()
            : base("AuthContext")
        {
           
        }
    }
}
