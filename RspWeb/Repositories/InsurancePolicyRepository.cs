﻿using RspWeb.Contexts;
using RspWeb.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RspWeb.Models;
using System.Data.Entity;

namespace RspWeb.Repositories
{
    public class InsurancePolicyRepository : IInsurancePolicyRepository
    {
        private AuthContext _ctx;
       
         public InsurancePolicyRepository()
        {
            _ctx = new AuthContext();
        }

        public async Task<IEnumerable<InsurancePolicy>> GetAll()
        {
              return await _ctx.Policies.OrderBy(p => p.Effective).Include(p => p.Insureds).Include(p => p.Risks).ToListAsync();
        }

        public async Task<InsurancePolicy> AddPolicy(InsurancePolicy policy)
        {
            _ctx.Policies.Add(policy);
            await _ctx.SaveChangesAsync();

            return policy;
        }

        public async Task<InsurancePolicy> GetById(int id)
        {
            return await _ctx.Policies.Include(p=>p.Insureds).Include(p=>p.Risks).FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<InsurancePolicy> Update(InsurancePolicy policy)
        {
            var oldPolicy = await GetById(policy.Id);

            if (oldPolicy != null)
            {

                oldPolicy.Expiration = policy.Expiration;
                oldPolicy.Effective = policy.Effective;
                oldPolicy.Insureds = policy.Insureds;
                oldPolicy.Risks = policy.Risks;
                
                await _ctx.SaveChangesAsync();
            }

            return oldPolicy;

        }


        public async Task Delete(int id)
        {
            var oldPolicy = await GetById(id);

            if (oldPolicy == null)
                throw new InvalidOperationException();

            _ctx.Policies.Remove(oldPolicy);
            await _ctx.SaveChangesAsync();
        }


        public void Dispose()
        {
            _ctx.Dispose();
        }

        public async Task<IEnumerable<InsurancePolicy>> Find(PolicySearch search)
        {
            string[] criteria = search.Criteria.Split(',');

            IQueryable<InsurancePolicy> query = null;

            bool searchByPolicyNumber = search.UsePolicyNumber.HasValue ? search.UsePolicyNumber.Value : false;
            bool searchByInsuredName = search.UseInsuredName.HasValue ? search.UseInsuredName.Value : false;
           

            return await query.ToListAsync();
        }
    }
}
