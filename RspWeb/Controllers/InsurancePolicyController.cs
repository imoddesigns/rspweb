﻿using RspWeb.Contracts;
using RspWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace RspWeb.Controllers
{
    [Authorize]
    [RoutePrefix("api/insurancepolicy")]
    public class InsurancePolicyController : ApiController
    {

        private readonly IInsurancePolicyRepository _repo;

        public InsurancePolicyController(IInsurancePolicyRepository repo)
        {
            _repo = repo;
        }

        // GET api/<controller>
        public async Task<IEnumerable<InsurancePolicy>> Get()
        {
            return await _repo.GetAll();
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<InsurancePolicy> Get(int id)
        {
           var policy = await _repo.GetById(id);

            if (policy != null)
                return policy;

            throw new HttpResponseException(HttpStatusCode.NotFound);

        }

        // POST api/<controller>
        public async Task<IHttpActionResult> Post([FromBody]InsurancePolicy policy)
        {
            var addedPolicy = await _repo.AddPolicy(policy);
            return Created<InsurancePolicy>(Request.RequestUri + addedPolicy.Id.ToString(), addedPolicy);
        }
        

        // PUT api/<controller>/5
        public async Task<IHttpActionResult> Put([FromBody]InsurancePolicy policy)
        {
            var updatedPolicy = await _repo.Update(policy);

            if (updatedPolicy == null)
                return NotFound();

            return Ok(updatedPolicy);

        }

        [Route("{id:int}")]
        [AcceptVerbs("DELETE")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
               await  _repo.Delete(id);
                return Ok();
            }
            catch(Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

        //[Route("find")]
        //[HttpGet]
        //public async Task<IEnumerable<InsurancePolicy>> Find([FromUri] PolicySearch search)
        //{
        //    try
        //    {
        //       return await _repo.Find(search);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(HttpStatusCode.BadRequest);
        //    }
        //} 
    }
}