namespace RspWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInsuredsAndRisksToPolicy : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Insureds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsPrimary = c.Boolean(nullable: false),
                        Name = c.String(),
                        MailingAddress = c.String(),
                        City = c.String(),
                        State = c.String(),
                        ZipCode = c.String(),
                        InsurancePolicy_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InsurancePolicies", t => t.InsurancePolicy_Id)
                .Index(t => t.InsurancePolicy_Id);
            
            CreateTable(
                "dbo.Risks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Construction = c.Int(nullable: false),
                        YearBuilt = c.Int(nullable: false),
                        Address = c.String(),
                        City = c.String(),
                        State = c.String(),
                        ZipCode = c.String(),
                        InsurancePolicy_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InsurancePolicies", t => t.InsurancePolicy_Id)
                .Index(t => t.InsurancePolicy_Id);
            
            DropColumn("dbo.InsurancePolicies", "PrimaryName");
            DropColumn("dbo.InsurancePolicies", "PrimaryMailingAddress");
            DropColumn("dbo.InsurancePolicies", "PrimaryCity");
            DropColumn("dbo.InsurancePolicies", "PrimaryState");
            DropColumn("dbo.InsurancePolicies", "PrimaryZipCode");
            DropColumn("dbo.InsurancePolicies", "RiskConstruction");
            DropColumn("dbo.InsurancePolicies", "RiskYearBuilt");
            DropColumn("dbo.InsurancePolicies", "RiskAddress");
            DropColumn("dbo.InsurancePolicies", "RiskCity");
            DropColumn("dbo.InsurancePolicies", "RiskState");
            DropColumn("dbo.InsurancePolicies", "RiskZipCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.InsurancePolicies", "RiskZipCode", c => c.String());
            AddColumn("dbo.InsurancePolicies", "RiskState", c => c.String());
            AddColumn("dbo.InsurancePolicies", "RiskCity", c => c.String());
            AddColumn("dbo.InsurancePolicies", "RiskAddress", c => c.String());
            AddColumn("dbo.InsurancePolicies", "RiskYearBuilt", c => c.Int(nullable: false));
            AddColumn("dbo.InsurancePolicies", "RiskConstruction", c => c.Int(nullable: false));
            AddColumn("dbo.InsurancePolicies", "PrimaryZipCode", c => c.String());
            AddColumn("dbo.InsurancePolicies", "PrimaryState", c => c.String());
            AddColumn("dbo.InsurancePolicies", "PrimaryCity", c => c.String());
            AddColumn("dbo.InsurancePolicies", "PrimaryMailingAddress", c => c.String());
            AddColumn("dbo.InsurancePolicies", "PrimaryName", c => c.String());
            DropForeignKey("dbo.Risks", "InsurancePolicy_Id", "dbo.InsurancePolicies");
            DropForeignKey("dbo.Insureds", "InsurancePolicy_Id", "dbo.InsurancePolicies");
            DropIndex("dbo.Risks", new[] { "InsurancePolicy_Id" });
            DropIndex("dbo.Insureds", new[] { "InsurancePolicy_Id" });
            DropTable("dbo.Risks");
            DropTable("dbo.Insureds");
        }
    }
}
