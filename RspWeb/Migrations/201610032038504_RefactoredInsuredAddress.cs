namespace RspWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoredInsuredAddress : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Insureds", "Address", c => c.String());
            DropColumn("dbo.Insureds", "MailingAddress");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Insureds", "MailingAddress", c => c.String());
            DropColumn("dbo.Insureds", "Address");
        }
    }
}
